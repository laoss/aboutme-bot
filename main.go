package main

import (
	"github.com/joho/godotenv"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	tele "gopkg.in/telebot.v3"
	"os"
	"strconv"
	"time"
)

func main() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal().Err(err).Msg("Cannot load env variables.")
	}

	setupLogger()

	bot, err := setupNewBot()
	if err != nil {
		log.Fatal().Err(err).Msg("Cannot setup new bot.")
	}

	commandHandler(bot)

	bot.Start()
}

func setupNewBot() (*tele.Bot, error) {
	pref := tele.Settings{
		Token:  os.Getenv("BOT_TOKEN"),
		Poller: &tele.LongPoller{Timeout: 10 * time.Second},
	}

	b, err := tele.NewBot(pref)
	if err != nil {
		return nil, err
	}
	return b, nil
}

func commandHandler(b *tele.Bot) {
	b.Handle(tele.OnText, func(c tele.Context) error {
		logSender(c)

		var msg string
		switch c.Text() {
		case "/about":
			msg = aboutMe
		case "/links":
			msg = socialLinks
		case "/help":
			msg = startNHelp
		case "/start":
			msg = startNHelp
		default:
			msg = defaultText
		}

		return c.Send(msg, tele.ModeHTML)
	})
}

func setupLogger() {
	if os.Getenv("LOG_PRETTY_ENABLE") == "1" {
		log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})
	}
	if os.Getenv("LOG_TIME_FORMAT_UNIX") == "1" {
		zerolog.TimeFieldFormat = zerolog.TimeFormatUnix
	}
}

func logSender(c tele.Context) {
	var (
		text = c.Text()
		user string
	)
	if c.Sender().Username != "" {
		user = c.Sender().Username
	} else {
		user = strconv.FormatInt(c.Sender().ID, 10) + "  " + c.Sender().FirstName
		if c.Sender().LastName != "" {
			user += " " + c.Sender().LastName
		}
	}

	log.Info().Msgf("user: %s, text: %s", user, text)
}
