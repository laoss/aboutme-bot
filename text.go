package main

const aboutMe = `<b>About me</b>
Чебан Станіслав - був народжений 12 серпня 2001 року, 
на півдні України, в смт Арбузинка, Південноукраїнського району, Миколаївської області.
Навчався в школах 3х різних міст України (не одночасно:)).
Писав цей текст десь вночі.`
const socialLinks = `<b>My social links</b>
GitLab   - https://gitlab.com/laoss
GitHub   - https://github.com/laosss
LinkedIn - https://www.linkedin.com/in/stanslav
Telegram - @stanisIav_ch`
const startNHelp = `<b>List of commands</b>
/about - short info about me
/links - list of my social links
/help - show list of commands`
const defaultText = `<b>Unknown command.</b>
Type\tap /help to get a list of commands.`
