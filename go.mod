module git.foxminded.com.ua/foxstudent102518/aboutme-bot

go 1.18

require github.com/joho/godotenv v1.4.0

require (
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.16 // indirect
	github.com/rs/zerolog v1.27.0 // indirect
	golang.org/x/sys v0.0.0-20220811171246-fbc7d0a398ab // indirect
	gopkg.in/telebot.v3 v3.0.0 // indirect
)
